/* Vue.config.devtools = true */
var app = new Vue({

  el: '#app',
  data: {
    page: 'menu1',
    menuHide: false,
    previusPage: ['manu1'],
    teams: [],
    matches: [],
    locations: [],
    news: [],
    selectedMatch: null,
    selectedTeam: null,
    matchLocation: {},
    dates: [],
    heightVp: 0,
    topHight: 0,
    forumPosts: [],
    logged: true,
    userId: '5RytHALuHEafHIEik5KAnVsxoEj2'
  },
  mounted() {
    // Register an event listener when the Vue component is ready
    window.addEventListener('resize', this.onResize);


    window.addEventListener("load", function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });


  },
  methods: {
    funcCom: function (postKey) {
      var commInput = document.getElementById("comm-input-" + postKey).value

      var aux = commInput.replace(/ /g, "")

      if (aux == "") {
        return alert("The comment must have some text on it!");
      } else {
        var username = firebase.auth().currentUser.displayName;
        var uid = firebase.auth().currentUser.uid;

        createNewComment(postKey, username, uid, commInput);
        document.getElementById("comm-input-" + postKey).value = '';
      }

    },
    mForum(id) {
      this.previusPage.push(this.page)

      this.page = "Match Forum";

      cleanupUi();
      startDatabaseQueries();

    },
    login() {
      let email = $("#email").val();
      let password = $("#pwd").val();
      firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
      });
    },
    register() {
      let email = $("#email").val();
      let password = $("#pwd").val();
      firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // ...
      });
    },
    onResize(event) {
      this.heightVp = ($(window).height() - $("#top").height()) + 'px';
      this.topHight = $("#top").height() + 'px';

    },
    setpage(page) {
      this.previusPage.push(this.page);
      this.page = page;
      document.documentElement.scrollTop = 0;
    },
    back() {
      if (this.previusPage.length > 1) {
        this.page = this.previusPage.pop();
      }
    },
    selectMatch(ID) {
      let match;
      let location;
      for (match of this.matches) {
        if (ID == match.matchID) {
          this.selectedMatch = match;
        }
      }
      for (location of this.locations) {
        if (this.selectedMatch.location_id == location.id) {
          this.matchLocation = location;
        }

      }

    },
    selectTeam(name) {
      for (team of this.teams) {
        if (name == team.team_name) {
          this.selectedTeam = team
        }
      }
    },
    getTeamShield(teamS) {

      for (team of this.teams) {
        if (teamS == team.team_name) {
          return team.team_logo_img;
        }
      }

    },
    sortMatches(array) {
      let arrayAux = array.slice(0);
      let months = [];
      let matchOnDate = [];
      let matchOnMonth = [];
      let matchSorted = [];

      for (let i = 0; i < array.length; i++) {
        if (!(months.includes(array[i].monthN))) {
          months.push(array[i].monthN)
        }
      }
      months.sort();

      for (let i = 0; i < arrayAux.length; i++) {
        let day = arrayAux[i].day;
        let month = arrayAux[i].monthN;
        let matchOnDay = [];

        matchOnDay.push(arrayAux[i]);
        arrayAux.splice(i, 1);
        i = 0;
        for (let y = 0; y < arrayAux.length; y++) {
          if (arrayAux[y].monthN == month && arrayAux[y].day == day) {
            matchOnDay.push(arrayAux[y]);
            arrayAux.splice(y, 1);
            y--;
          }
        }
        matchOnDate.push(matchOnDay);
      }

      for (let i = 0; i < months.length; i++) {
        let month = months[i];
        for (let y = 0; y < matchOnDate.length; y++) {

          if (matchOnDate[y][0].monthN == month) {
            matchOnMonth.push(matchOnDate[y])
          }
        }
        matchSorted.push(matchOnMonth);
        matchOnMonth = [];
      }
      this.dates = matchSorted;
    },
    nextTeamGame(teamName) {
      let now = new Date();
      for (match of this.matches) {
        let matchDate = new Date("2020", match.monthN, match.day, match.timeH, match.timeM);
        if (match.team1.team_name == teamName || match.team2.team_name == teamName) {
          if (now < matchDate) {
            return match;
          } else { return null; }
        }
      }
    },
    nextGame() {
      let now = new Date();
      for (match of this.matches) {
        let matchDate = new Date("2020", match.monthN, match.day, match.timeH, match.timeM);
        if (now < matchDate) {
          return match;
        } else { return null; }
      }
    },
    isUser(userId) {
      if (userId == this.userId) {
        return true;
      } else { return false; }
    },
    getDate(date) {
      return new Date(date).toLocaleString()
    }
  },
  computed: {

  },
  created() {
    this.teams = info.teams;
    this.matches = info.matches;
    this.locations = info.locations;
    this.news = info.news;
    this.sortMatches(info.matches);
    this.heightVp = ($(window).height() - $("#top").height()) + 'px';
    this.topHight = $("#top").height() + 'px';
    for (let i in this.matches) {
      for (let z in this.teams) {
        if (this.teams[z].team_name == this.matches[i].team1) {
          this.matches[i].team1 = this.teams[z]
        }
        if (this.teams[z].team_name == this.matches[i].team2) {
          this.matches[i].team2 = this.teams[z]
        }
      }
      for (let z in this.locations) {
        if (this.locations[z].id == this.matches[i].location_id) {
          this.matches[i].location = this.locations[z]
        }
      }
    }
  }
})
$("#menuTog").click(function () {
  let hide = app.menuHide;
  let height;
  if (hide) {
    height = $("#top").height() + 44;
    $("#navbar").css("height", "44px");
    app.menuHide = false;
  } else {
    height = $("#top").height() - 44;
    $("#navbar").css("height", "0px");
    app.menuHide = true;
  }
  app.topHight = height + 'px';
  app.heightVp = ($(window).height() - height + 'px');
  console.log("CLICK");
})

